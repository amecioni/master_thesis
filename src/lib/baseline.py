import numpy as np
from scipy.sparse import csc_matrix
from scipy.sparse import spdiags
from scipy.sparse.linalg import spsolve

def als(y, lam, p, niter = 10):
    L = len(y)
    D = csc_matrix(np.diff(np.eye(L), 2))
    w = np.ones(L)
    for i in range(niter):
        W = spdiags(w, 0, L, L)
        Z = W + lam * D.dot(D.transpose())
        z = spsolve(Z, w*y)
        w = p * (y > z) + (1-p) * (y < z)
    return z
