import csv
import os
import re
import glob
import math
import numpy as np
from scipy.sparse import csc_matrix
from scipy.sparse import spdiags
from scipy.sparse.linalg import spsolve
from sklearn.model_selection import train_test_split
from .baseline import als

class Sample(object):
    def __init__(
            self, wavenumbers, intensities, 
            filename=None, label=None, tags=None, meta=None):
        if len(wavenumbers) != len(intensities):
            raise ValueError('Wavenumber and intensity value counts do not match')
        self.wavenumbers = wavenumbers
        self.intensities = intensities
        self.filename = filename
        self.label = label
        self.tags = tags
        self.meta = meta
        # Determine wavenumber step
        n = len(self.wavenumbers)
        steps = np.roll(self.wavenumbers, -1)[:n-1] - self.wavenumbers[:n-1]
        self._wn_step = np.mean(steps)      # Might not be 100% consistent

    def __len__(self):
        return len(self.wavenumbers)

    @property
    def wn_min(self):
        return np.min(self.wavenumbers)

    @property
    def wn_max(self):
        return np.max(self.wavenumbers)

    @property
    def wn_step(self):
        return self._wn_step

    def subtract_baseline(self):
        baseline = als(self.intensities, 10000, 0.001)
        subtracted = self.intensities - baseline
        return Sample(
            self.wavenumbers, subtracted, 
            filename=self.filename, label=self.label,
            tags=self.tags, meta=self.meta)

    def interpolate(self, wn_min, wn_max, wn_step):
        wavenumbers = np.arange(wn_min, wn_max + wn_step, wn_step)
        intensities = np.interp(wavenumbers, self.wavenumbers, self.intensities)
        return Sample(
            wavenumbers, intensities, 
            filename=self.filename, label=self.label,
            tags=self.tags, meta=self.meta)


class Dataset(object):
    def __init__(self, samples, label=None, meta=None):
        self._samples = samples
        self._update_properties()
        self.label = label
        self.meta = meta if meta else {}

    def __add__(self, other):
        return Dataset(self._samples + other._samples)

    def __iadd__(self, other):
        self._samples += other._samples
        self._update_properties()
        return self

    def __len__(self):
        return len(self._samples)

    def __getitem__(self, key):
        if isinstance(key, int):
            return self._samples[key]
        elif isinstance(key, slice):
            return Dataset(self._samples[key], self.label, self.meta)

    def __iter__(self):
        return self._samples.__iter__()

    @property
    def wn_min(self):
        return self._wn_min

    @property
    def wn_max(self):
        return self._wn_max

    @property
    def wavenumbers(self):
        return self._wavenumbers

    @property
    def intensities(self):
        return self._intensities

    @property
    def consistent(self):
        '''
        Check if wavenumbers of all samples match
        '''
        for idx in range(len(self._samples) - 1):
            pair_consistent = np.array_equiv(
                self._samples[idx].wavenumbers,
                self._samples[idx + 1].wavenumbers,)
            if not pair_consistent:
                return False
        return True

    def take_random(self):
        raise NotImplementedError

    def interpolate(self, wn_min, wn_max, wn_step):
        samples = [sample.interpolate(wn_min, wn_max, wn_step)
            for sample in self._samples]
        return Dataset(samples, self.label, self.meta)

    def subtract_baseline(self):
        samples = [sample.subtract_baseline() for sample in self._samples]
        return Dataset(samples, self.label, self.meta)

    def _update_properties(self):
        if self.consistent and len(self._samples) > 0:
            self._wn_min = np.min(self._samples[0].wavenumbers)
            self._wn_max = np.max(self._samples[0].wavenumbers)
            self._wavenumbers = self._samples[0].wavenumbers
            self._intensities = np.array([
                sample.intensities for sample in self._samples])
        else:
            self._wn_min = None
            self._wn_max = None
            self._wavenumbers = None
            self._intensities = None

    def split(self, test_size=0.5):
        '''
        Train/test split 
        '''
        train_samples, test_samples = train_test_split(
            self._samples, test_size=test_size)
        train_set = Dataset(train_samples, self.label, self.meta)
        test_set = Dataset(test_samples, self.label, self.meta)
        return train_set, test_set


def _read_files(path_pattern, num_files=None, delimiter=','):
    meta = []
    data = []
    files = glob.glob(path_pattern)
    pat = re.compile('^#+([^=]+)=(.*)$')

    if num_files is None or num_files > len(files):
        j = len(files)
    else:
        j = num_files if num_files > 0 else 0
    
    i = 0
    f = math.ceil((1.0 * j) / 10)
    
    for file in files:
        if i < j:
            # Get all lines, leave comments only
            coms = [ re.findall(r'^#.*',line) for line in open(file) ]

            # Flatten list, leave not empty only
            coms = [ x[0] for x in coms if len(x) > 0 ]

            # Split comments to keys and values
            coms = dict((pat.sub(r'\1', k),pat.sub(r'\2', k)) for k in coms)

            # Read data
            vals = np.genfromtxt(file, delimiter=delimiter, comments = '#')

            # Ensure that wavenumbers increase
            vals = vals[np.argsort(vals[:, 0])]

            coms['PATH'] = file
            meta.append(coms)
            data.append(vals)

            i = i + 1
        
        # if f == 0 or (i % f) == 0 or i == j:
        #     print ('[{0}/{1}/{2}]'.format(i, j, len(files))), "loaded"
            
        if i == j: break
    return data, meta


def read_minerals(path_pattern):
    data, meta = _read_files(path_pattern, delimiter=',')
    samples = [
        Sample(
            np.array(data[idx][:, 0]),
            np.array(data[idx][:, 1]), 
            # Label is mineral name specified in comments
            meta[idx]['NAMES'], meta)
        for idx in range(len(data))]

    return Dataset(samples)


def read_organics(path_pattern):
    data, meta = _read_files(path_pattern, delimiter=' ')
    # Treat filenames as labels
    labels = [
        os.path.basename(m['PATH']).split('.')[0]
        for m in meta]

    samples = [
        Sample(
            np.array(data[idx][:, 0]),
            np.array(data[idx][:, 1]), 
            labels[idx], meta)
        for idx in range(len(data))]

    return Dataset(samples)


def read_csv(input_path):
    samples, wavenumbers = [], None
    with open(input_path) as f:
        reader = csv.reader(f)
        header = next(reader)

        # Collect wavenumbers from header
        wavenumbers = []
        for col_name in header:
            try:
                float(col_name)
            except:
                pass
            else:
                wavenumbers.append(float(col_name))
        wavenumbers = np.array(wavenumbers)

        # Sort wavenumbers in ascending order
        ordering = np.argsort(wavenumbers)
        wavenumbers = wavenumbers[ordering]

        wn_indices = []
        for wavenumber in wavenumbers:
            # Try finding wavenumbers either by numbers or strings
            try:
                wn_idx = next(
                    idx for idx in range(len(header))
                    if header[idx].startswith(str(wavenumber)))
            except StopIteration:
                wn_idx = next(
                    idx for idx in range(len(header))
                    if header[idx] == str(int(wavenumber)))
            
            wn_indices.append(wn_idx)

        for row in reader:
            intensities = np.array([
                row[wn_idx] for wn_idx in wn_indices]).astype(np.float64)
            tags = row[header.index('tags')].split('/')
            sample = Sample(
                wavenumbers, intensities,
                label=row[header.index('label')],
                tags=tags,
                filename=row[header.index('filename')],
                meta=row[header.index('metadata')])
            samples.append(sample)

    return Dataset(samples)


def write_csv(dataset, output_path):
    fieldnames = ['label', 'tags', 'filename', 'metadata']
    fieldnames += dataset.wavenumbers.astype(str).tolist()
    with open(output_path, 'w') as f:
        writer = csv.DictWriter(f, fieldnames=fieldnames)
        writer.writeheader()
        for sample in dataset:
            d = {
                'label': sample.label,
                'filename': sample.filename,
                'metadata': sample.meta, }
            if sample.tags:
                d['tags'] = '/'.join(sample.tags)
            else:
                d['tags'] = ''
            d.update(zip(
                sample.wavenumbers.astype(str), 
                sample.intensities))
            writer.writerow(d)


if __name__ == '__main__':
    from plots import plot_dataset, plot_datasets
    from baseline import als
    '''
    dataset1 = read_organics(
        '/home/tomas/Dropbox/Raman_duomenys/Kalakutiena/1 kalakutiena slaunis oda/*.dat')
    dataset2 = read_organics(
        '/home/tomas/Dropbox/Raman_duomenys/Kiauliena/5 kumpis/*.dat')

    dataset1 = Dataset([
        sample.subtract_baseline() for sample in dataset1])
    dataset2 = Dataset([
        sample.subtract_baseline() for sample in dataset2])
    plot_datasets([dataset1, dataset2])
    '''

    # s1 = dataset1[0]
    # s2 = s1.subtract_baseline()
    # plot_dataset([s1, s2])
    # plot_datasets([dataset1, dataset2])
