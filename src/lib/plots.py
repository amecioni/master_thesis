import itertools
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

def plot_dataset(dataset):
    matplotlib.rc('font', size=14)
    matplotlib.rc('axes', labelsize=17)
    plt.figure(figsize=(9, 6))
    for sample in dataset:
        plt.plot(sample.wavenumbers, sample.intensities)
    plt.xlabel('Ramano poslinkis (cm$^{-1}$)')
    plt.ylabel('Intensyvumas (sant. vnt.)')
    plt.show()
    
def plot_confusion_matrix(cm, classes):
    matplotlib.rc('font', size=14)
    matplotlib.rc('axes', labelsize=17)
    plt.figure(figsize=(9, 9))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], 'd'),
                horizontalalignment="center",
                color="white" if cm[i, j] > thresh else "black")

    plt.ylabel('Tikroji reikšmė')
    plt.xlabel('Modeliu nustatyta reikšmė')
    plt.show()